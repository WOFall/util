// Prefer chtimes_unix if possible; assume touch isn't available on windows.
// +build !linux,!netbsd,!openbsd,!windows static
// +build !notouch

package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"time"
)

func init() {
	// check touch exists, and accepts non-posix --no-dereference
	cmd := exec.Command("touch", "--no-dereference", "-")
	if err := cmd.Run(); err != nil {
		return
	}
	chtimes = touch
}

func touch(name string, atime time.Time, mtime time.Time) error {
	info, err := os.Lstat(name)
	if err != nil {
		return err
	}
	if info.Mode()&os.ModeSymlink == 0 {
		return os.Chtimes(name, atime, mtime)
	}

	cmd := exec.Command("touch", "--no-dereference", "-c", "-m", "-d", mtime.Format("2006-01-02T15:04:05.999999999Z"), "--", name)
	var errbuf bytes.Buffer
	cmd.Stderr = &errbuf

	err = cmd.Run()
	switch ee := err.(type) {
	case nil:
	case *exec.ExitError:
		fmt.Fprintf(os.Stderr, "touch %s: %s", ee.Error(), errbuf.String())
	default:
		log.Fatal(err)
	}
	return nil
}
