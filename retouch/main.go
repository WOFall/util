// This script recursively resets the modify time of each directory to the most
// recent modify time of all its children. Mainly intended for use when a
// temporary file (such as those created when viewing a file in an editor)
// updates the timestamp of the containing directory, thereby "breaking" ls -t.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"
)

var chtimes = os.Chtimes // potentially changed by chtimes_unix or chtimes_touch

var (
	flagAdvance  = flag.Bool("advance", true, "allow times to be advanced")
	flagDry      = flag.Bool("dry", false, "supress changes")
	flagNodot    = flag.Bool("nodot", false, "ignore .dot files (except paths listed as arguments)")
	flagQuiet    = flag.Bool("quiet", false, "supress summary of changes")
	flagRewind   = flag.Bool("rewind", true, "allow times to be rewound")
	flagSymlinks = flag.Bool("symlinks", true, "follow symlinks")
)

var symlinkDepth = 64

func init() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "usage: %s [flags] DIR...\n%s\n", os.Args[0],
			"Recursively set each DIR's modify time to that of its most recently modified child.")
		flag.PrintDefaults()
	}
}

func main() {
	flag.Parse()
	paths := flag.Args()

	if !*flagAdvance && !*flagRewind {
		fmt.Fprintln(os.Stderr, "no action possible with both -advance and -rewind disabled")
		os.Exit(1)
	}

	if len(paths) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	for _, path := range paths {
		sweepDir(path)
	}
}

// ignore boring errors, die on bad ones
func nonfatal(err error) bool {
	if err != nil {
		if os.IsNotExist(err) || os.IsPermission(err) {
			fmt.Fprintln(os.Stderr, err)
			return true
		} else {
			log.Fatal(err)
		}
	}
	return false
}

func sweepDir(dir string) (time.Time, error) {
	stat, err := os.Lstat(dir)
	if nonfatal(err) {
		return time.Time{}, err
	}
	mtime := stat.ModTime()

	if stat.Mode()&os.ModeSymlink != 0 && *flagSymlinks {
		if symlinkDepth--; symlinkDepth <= 0 {
			fmt.Fprintln(os.Stderr, "Error: symlink recursion limit reached:", dir)
			os.Exit(2)
		}
		name, err := os.Readlink(dir)
		if nonfatal(err) {
			return time.Time{}, err
		}
		if !filepath.IsAbs(name) {
			name = filepath.Join(filepath.Dir(dir), name)
		}

		maxTime, err := sweepDir(name)
		if err != nil {
			return maxTime, err
		}
		setTime(dir, stat, mtime, maxTime)
		symlinkDepth++
		return maxTime, nil

	}

	if !stat.IsDir() {
		return mtime, nil
	}

	f, err := os.Open(dir)
	if nonfatal(err) {
		return mtime, err
	}

	infos, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		return mtime, err
	}

	var maxTime time.Time
	processed := false

	for _, info := range infos {
		name := info.Name()
		if *flagNodot && len(name) > 1 && name[0] == '.' {
			continue
		}
		name = filepath.Join(dir, name)

		var t time.Time
		if info.IsDir() || info.Mode()&os.ModeSymlink != 0 && *flagSymlinks {
			t, err = sweepDir(name)
			if err != nil {
				continue
			}
		} else {
			t = info.ModTime()
		}

		if t.After(maxTime) {
			maxTime = t
		}
		processed = true
	}

	// empty directory, all files ignored, or all files errors
	if !processed {
		return mtime, nil
	}

	setTime(dir, stat, mtime, maxTime)
	return maxTime, nil
}

func setTime(dir string, stat os.FileInfo, oldT, newT time.Time) {
	if !(newT.Before(oldT) && *flagRewind || newT.After(oldT) && *flagAdvance) {
		return // either no adjustment, or direction not allowed
	}

	if !*flagQuiet {
		diff := newT.Sub(oldT)
		diff -= diff % time.Second // round
		fmt.Printf("%s %15s %s\n", newT.Format("2006-01-02 15:04:05"), diff, dir)
	}

	if !*flagDry {
		// finally, update the timestamp
		err := chtimes(dir, time.Now(), newT)
		nonfatal(err)
	}
}
