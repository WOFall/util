// Copyright 2014 Gavin Troy <gavtroy@fastmail.fm>.
// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// SYS_UTIMENSAT is not currently defined for darwin, dragonfly, or freebsd.
// +build linux netbsd openbsd
// +build !static

// Set chtimes to a version that doesn't dereference symlinks.
// Mostly copied straight from "os" and "syscall", adding AT_SYMLINK_NOFOLLOW.

package main

import (
	"os"
	. "syscall"
	"time"
	"unsafe"
)

/*
#include <fcntl.h>
#include <sys/stat.h>
*/
import "C"

func init() {
	chtimes = lchtimes
}

// copied from os.Chtimes; use "lutimesNano" instead of syscall.UtimesNano
func lchtimes(name string, atime time.Time, mtime time.Time) error {
	var utimes [2]Timespec
	utimes[0] = NsecToTimespec(atime.UnixNano())
	utimes[1] = NsecToTimespec(mtime.UnixNano())
	if e := lutimesNano(name, utimes[0:]); e != nil {
		return &os.PathError{"chtimes", name, e}
	}
	return nil
}

// combination of _linux and _unix syscall.UtimesNano
func lutimesNano(path string, ts []Timespec) (err error) {
	if len(ts) != 2 {
		return EINVAL
	}
	err = utimensat(C.AT_FDCWD, path, (*[2]Timespec)(unsafe.Pointer(&ts[0])))
	if err != ENOSYS {
		return err
	}
	// If the utimensat syscall isn't available (utimensat was added to Linux
	// in 2.6.22, Released, 8 July 2007) then fall back to utimes
	tv := [2]Timeval{
		NsecToTimeval(TimespecToNsec(ts[0])),
		NsecToTimeval(TimespecToNsec(ts[1])),
	}
	return utimes(path, (*[2]Timeval)(unsafe.Pointer(&tv[0])))
}

// _linux syscall.utimes
func utimes(path string, times *[2]Timeval) (err error) {
	var _p0 *byte
	_p0, err = BytePtrFromString(path)
	if err != nil {
		return
	}
	_, _, e1 := Syscall(SYS_UTIMES, uintptr(unsafe.Pointer(_p0)), uintptr(unsafe.Pointer(times)), 0)
	if e1 != 0 {
		err = e1
	}
	return
}

// _linux syscall.utimensat; added flag AT_SYMLINK_NOFOLLOW
func utimensat(dirfd int, path string, times *[2]Timespec) (err error) {
	var _p0 *byte
	_p0, err = BytePtrFromString(path)
	if err != nil {
		return
	}
	_, _, e1 := Syscall6(SYS_UTIMENSAT, uintptr(dirfd), uintptr(unsafe.Pointer(_p0)), uintptr(unsafe.Pointer(times)), C.AT_SYMLINK_NOFOLLOW, 0, 0)
	if e1 != 0 {
		err = e1
	}
	return
}
