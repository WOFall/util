// Generates a stupidly simple html crib for a set of images, because browsers
// make fairly good continuous image viewers. (Compare to zathura, which has
// rather poor scrolling performance.)
package main

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"os"
	"path"
	"regexp"
	"sort"
)

var (
	cribTmpl     = template.Must(template.New("cribTmpl").Parse(crib_tmpl))
	imagePattern = regexp.MustCompile(`\.(?i)(png|jpe?g|bmp|gif|webp|svg)$`)
)

func must(err interface{}) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	if len(os.Args) > 1 {
		for _, dir := range os.Args[1:] {
			makeCrib(dir)
		}
	} else {
		dirname, err := os.Getwd()
		must(err)
		makeCrib(dirname)
	}
}

func makeCrib(dirname string) {
	var crib bytes.Buffer

	dir, err := os.Open(dirname)
	must(err)
	infos, err := dir.Readdir(-1)
	must(err)

	names := make([]string, 0, len(infos))
	for _, fi := range infos {
		name := fi.Name()
		if fi.IsDir() || len(name) > 1 && name[0] == '.' {
			continue
		}
		if !imagePattern.MatchString(name) {
			continue
		}
		names = append(names, name)
	}

	if len(names) == 0 {
		fmt.Println("No images found in", dirname)
		return
	}

	sort.Strings(names)

	must(cribTmpl.ExecuteTemplate(&crib, "cribTmpl", names))

	filename := path.Join(dirname, "images.html")
	must(ioutil.WriteFile(filename, crib.Bytes(), 0644))
}

var crib_tmpl = `<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Image view</title>
		<style>
		body {
			margin: 0;
		}
		img {
			display: block;
			margin-left: auto;
			margin-right: auto;
			border-width: 0;
		}
		</style>
	</head>
	<body>{{range .}}
		<img alt="x" src="{{.}}"/>{{end}}
	</body>
</html>
`
